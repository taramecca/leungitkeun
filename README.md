# Tugas 2 PPW Kelompok C Semester Genap 2017/2018 (kelas pak Gladhi)

Nama-nama Anggota Kelompok:
1. Mohammad Raffi Akbar (Paket A fitur 2, 3, dan 4)
2. Muzakki H. Azmi (-)
3. Tara Mecca Luna (Paket A fitur 1)


Status Pipelines:
![pipeline status](https://gitlab.com/keraktelor/leungitkeun/badges/master/pipeline.svg)

Coverage Report:
![coverage report](https://gitlab.com/keraktelor/leungitkeun/badges/master/coverage.svg)


Link Herokuapp:
http://leungitkeun.herokuapp.com/
